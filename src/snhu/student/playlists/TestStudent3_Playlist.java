package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class TestStudent3_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> deadmau5Tracks = new ArrayList<Song>();
	Deadmau5 deadmau5 = new Deadmau5();
	
	deadmau5Tracks = deadmau5.getDeadmau5Songs();
	
	playlist.add(deadmau5Tracks.get(0));
	playlist.add(deadmau5Tracks.get(1));
	
    return playlist;
	}
}