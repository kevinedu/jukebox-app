package snhu.student.playlists;

import snhu.jukebox.playlist.PlayableSong;
import snhu.jukebox.playlist.Song;
import music.artist.*;
import java.util.ArrayList;
import java.util.LinkedList;

public class KevinNichols_Playlist {
    
	public LinkedList<PlayableSong> StudentPlaylist(){
	
	LinkedList<PlayableSong> playlist = new LinkedList<PlayableSong>();
	ArrayList<Song> bonesTracks = new ArrayList<Song>();
	Bones bones = new Bones();
	
	bonesTracks = bones.getBonesSongs();
	
	playlist.add(bonesTracks.get(0));
	playlist.add(bonesTracks.get(1));
	playlist.add(bonesTracks.get(2));
	playlist.add(bonesTracks.get(3));
	playlist.add(bonesTracks.get(4));
	
    return playlist;
	}
}