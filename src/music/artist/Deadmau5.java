package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class Deadmau5 {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Deadmau5() {
    }
    
    public ArrayList<Song> getDeadmau5Songs() {
    	
    	 albumTracks = new ArrayList<Song>();                                   //Instantiate the album so we can populate it below
    	 Song track1 = new Song("I Remember", "deadmau5");             //Create a song
         Song track2 = new Song("Faxing Berlin", "deadmau5");         //Create another song
         this.albumTracks.add(track1);                                          //Add the first song to song list for the deadmau5
         this.albumTracks.add(track2);                                          //Add the second song to song list for the deadmau5 
         return albumTracks;                                                    //Return the songs for the deadmau5 in the form of an ArrayList
    }
}