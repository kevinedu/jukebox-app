package music.artist;

import java.util.ArrayList;

import snhu.jukebox.playlist.Song;

public class Bones {
	
	ArrayList<Song> albumTracks;
    String albumTitle;
    
    public Bones() {
    }
    
    public ArrayList<Song> getBonesSongs() {
    	
    	 albumTracks = new ArrayList<Song>();              //Instantiate the album so we can populate it below
    	 Song track1 = new Song("HDMI", "BONES");          //Create a song
         Song track2 = new Song("Oxygen", "BONES");        //Create another song
         Song track3 = new Song("Carhartt", "BONES");
         Song track4 = new Song("BlastZone", "BONES");
         Song track5 = new Song("Iron", "BONES");
         this.albumTracks.add(track1);            //Add the first song to song list for BONES
         this.albumTracks.add(track2);            //Add the second song to song list for BONES
         this.albumTracks.add(track3);            //Add the third song to song list for BONES
         this.albumTracks.add(track4);            //Add the fourth song to song list for BONES
         this.albumTracks.add(track5);  		  //Add the fifth song to song list for BONES
         return albumTracks;                      //Return the songs for the deadmau5 in the form of an ArrayList
    }
}